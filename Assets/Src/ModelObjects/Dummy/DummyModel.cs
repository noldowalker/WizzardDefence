﻿using System.Collections;
using System.Collections.Generic;

namespace GameModels
{
    public class DummyModel : BaseModel
    {
        public DummyModel(float customHitPoints, float damage) : base(customHitPoints, damage) {
        }
    }
}
